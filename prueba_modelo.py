import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
import pandas as pd
from sklearn.metrics import f1_score

# Definir la arquitectura de la red neuronal
class GuanteNN(nn.Module):
    def __init__(self):
        super(GuanteNN, self).__init__()
        self.input_size = 24  # Número de sensores
        self.output_size = 6  # Salida

        self.fc1 = nn.Linear(self.input_size, 256)
        self.fc2 = nn.Linear(256, 128)
        self.fc3 = nn.Linear(128, 64)
        self.fc4 = nn.Linear(64, self.output_size)
        self.relu = nn.ReLU()

    def forward(self, x):
        out = self.relu(self.fc1(x))
        out = self.relu(self.fc2(out))
        out = self.relu(self.fc3(out))
        out = self.fc4(out)
        return out

class Dataset(torch.utils.data.Dataset):
    'Characterizes a dataset for PyTorch'
    def __init__(self, file_path): 
        df = pd.read_excel(file_path, header=None, names=['Letra', 'Ax', 'DAx', 'Ay', 'DAy', 'Az', 'DAz', 'Asqrt', 'DAsqrt', 'Gx', 'DGx', 'Gy', 'DGy', 'Gz', 'DGz', 'Meñique', 'DMeñique', 'Anular', 'DAnular', 'Medio', 'DMedio', 'Indice', 'DIndice' , 'Pulgar', 'DPulgar'])
        df['Letra'] = df['Letra'].replace({'A':0, 'E':1, 'I':2, 'O':3, 'U':4, 'Ñ':5})
        self.values = df[['Ax', 'DAx', 'Ay', 'DAy', 'Az', 'DAz', 'Asqrt', 'DAsqrt', 'Gx', 'DGx', 'Gy', 'DGy', 'Gz', 'DGz', 'Meñique', 'DMeñique', 'Anular', 'DAnular', 'Medio', 'DMedio', 'Indice', 'DIndice' , 'Pulgar', 'DPulgar']].values
        self.labels = (df['Letra']).astype(int).values
        self.values = torch.tensor(self.values, dtype=torch.float32)
        self.labels = torch.tensor(self.labels, dtype=torch.long)

    def __len__(self):
        'Denotes the total number of samples'
        return len(self.values)

    def __getitem__(self, index):
        'Generates one sample of data'
        return self.values[index], self.labels[index]
# Crear la red neuronal y moverla a la GPU si está disponible
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model = GuanteNN().to(device)

# Cosas por definir
classes = ['A', 'E', 'I', 'O', 'U', 'Ñ']
counts = {'A': 0, 'E': 0, 'I': 0, 'O': 0, 'U': 0, 'Ñ': 0}
true_labels = []
predicted_labels = []

# Ejemplo de predicción con nuevos datos
file_path = "C:/Users/Ignacio/Desktop/data_test.xlsx"  
dataset = Dataset(file_path)

# Carga del modelo
model.load_state_dict(torch.load("C:/Users/Ignacio/Desktop/best_modelo200.pt"))
model.eval()

for num_fila in dataset:
    with torch.no_grad():
        new_data, label = num_fila
        new_data = new_data.to(device)
        output = model(new_data.unsqueeze(0))
        predicted_label = torch.argmax(output, dim=1).item()
        predicted_class = classes[predicted_label]
        counts[predicted_class] += 1
        true_labels.append(label.item())
        predicted_labels.append(predicted_label)

# for clase, count in counts.items():
#     print(f'{clase}: {count}')
print('Resultados totales:', counts)

f1 = f1_score(true_labels, predicted_labels, average='weighted')
print("F1-Score:", f1)