import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

# Definir la arquitectura de la red neuronal
class GuanteNN(nn.Module):
    def __init__(self):
        super(GuanteNN, self).__init__()
        self.input_size = 24
        self.output_size = 2  

        self.fc1 = nn.Linear(self.input_size, 256)
        self.fc2 = nn.Linear(256, 128)
        self.fc3 = nn.Linear(128, 64)
        self.fc4 = nn.Linear(64, self.output_size)
        self.relu = nn.ReLU()

    def forward(self, x):
        out = self.relu(self.fc1(x))
        out = self.relu(self.fc2(out))
        out = self.relu(self.fc3(out))
        out = self.fc4(out)
        return out

class Dataset(torch.utils.data.Dataset):
  'Characterizes a dataset for PyTorch'
  def __init__(self, file_path): 
    # df = pd.read_excel(file_path, header=None, names=['Letra', 'Meñique', 'Anular', 'Medio', 'Índice', 'Pulgar'])
    df = pd.read_excel(file_path, header=None, names=['Letra', 'Ax', 'DAx', 'Ay', 'DAy', 'Az', 'DAz', 'Asqrt', 'DAsqrt', 'Gx', 'DGx', 'Gy', 'DGy', 'Gz', 'DGz', 'Meñique', 'DMeñique', 'Anular', 'DAnular', 'Medio', 'DMedio', 'Indice', 'DIndice' , 'Pulgar', 'DPulgar'])
    df['Letra'] = df['Letra'].replace({'A':0, 'Ñ': 1})
    self.values = df[['Ax', 'DAx', 'Ay', 'DAy', 'Az', 'DAz', 'Asqrt', 'DAsqrt', 'Gx', 'DGx', 'Gy', 'DGy', 'Gz', 'DGz', 'Meñique', 'DMeñique', 'Anular', 'DAnular', 'Medio', 'DMedio', 'Indice', 'DIndice' , 'Pulgar', 'DPulgar']].values
    self.labels = (df['Letra']).astype(int).values
    self.values = torch.tensor(self.values, dtype=torch.float32)
    self.labels = torch.tensor(self.labels, dtype=torch.long)

  def __len__(self):
        'Denotes the total number of samples'
        return len(self.values)

  def __getitem__(self, index):
        'Generates one sample of data'

        return self.values[index], self.labels[index]

# Definir hiperparámetros y cargar datos
learning_rate = 1e-3
num_epochs = 14
batch_size = 128
train_losses = []
valid_losses = []

# Crear la red neuronal y moverla a la GPU si está disponible
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model = GuanteNN().to(device)

# Definir la función de pérdida y el optimizador
criterion = nn.CrossEntropyLoss()
optimizer = optim.Adam(model.parameters(), lr=learning_rate)

# Generar datos de entrenamiento
# file_path = "/home/ignacio/Escritorio/xd3.xlsx"  
file_path = "C:/Users/Ignacio/Desktop/datos_train.xlsx"  
dataset = Dataset(file_path)

index = 0.8
train_dataset, valid_dataset = torch.utils.data.random_split(dataset, [int(len(dataset)*index), len(dataset) - int(len(dataset)*index)])

# Convertir a tensores y mover a la GPU si está disponible
train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size , shuffle = True)

valid_loader = torch.utils.data.DataLoader(valid_dataset, batch_size=batch_size , shuffle = True)

# Entrenamiento de la red neuronal
global_loss = np.inf

for epoch in range(num_epochs):
    train_loss = 0.0
    valid_loss = 0.0
    running_corrects = 0.0
    for x, y in train_loader:
        # Forward pass
        outputs = model(x.to(device))
        
        # Calcular la pérdida
        loss = criterion(outputs, y.to(device))
        train_loss=train_loss+loss.item()

        # Backward y optimización
        optimizer.zero_grad()
        loss.backward(retain_graph=True)
        optimizer.step()

    avg_train_loss = train_loss / len(train_loader)

    if (epoch+1) % 1 == 0:
        print(f'Epoch_train [{epoch+1}/{num_epochs}], Loss: {train_loss/(len(train_loader)*batch_size):.10f}')

    for x, y in valid_loader:
        outputs = model(x.to(device))
        loss = criterion(outputs, y.to(device))
        valid_loss=valid_loss+loss.item()
        _, preds = torch.max(outputs, 1)
        running_corrects += torch.sum(preds == y.to(device).data)

    avg_valid_loss = valid_loss / len(valid_loader)
    train_losses.append(avg_train_loss)
    valid_losses.append(avg_valid_loss)

    if (epoch+1) % 1 == 0:
        print(f'Epoch_valid [{epoch+1}/{num_epochs}], Loss: {valid_loss/(len(valid_loader)*batch_size):.10f}')
        print(f'Epoch_valid [{epoch+1}/{num_epochs}], Acc: {running_corrects/(len(valid_loader)*batch_size):.10f}')
    if valid_loss < global_loss:
        global_loss = valid_loss
        torch.save(model.state_dict(), 'pesos_nuevos.pt')

# Gráfica train_loss, valid_loss y Acc
# print('Pérdida de entrenamiento: ', min(train_losses), 'Pérdida de validación: ', min(valid_losses), 'Precisión: ', max(accuracies))
plt.figure(figsize=(12, 6))
plt.plot(range(1, num_epochs + 1), train_losses, label='Train Loss')
plt.plot(range(1, num_epochs + 1), valid_losses, label='Valid Loss')
plt.xlabel('Epoch')
plt.ylabel('Loss')
plt.legend()
plt.title('Train and Valid Losses')
plt.tight_layout()
plt.show()