import openpyxl
import serial

# Nuevos límites para procesamiento por lotes
batch_size = 100  # Número de datos a procesar y escribir por lote

# Archivo de Excel
excel_file_path = "C:/Users/Ignacio/Desktop/raw.xlsx"

# Puerto serial
serial_port = 'COM12'
baud_rate = 115200

# Conectar al puerto serial
ser = serial.Serial(serial_port, baud_rate)

# Abre el archivo de Excel
workbook = openpyxl.load_workbook(excel_file_path)
sheet = workbook.active

count = 0
max_valores = 100
datos_completos = []

try:
    # persona = str(input("Qué persona está con el guante?: "))
    persona = str("Cecilia")
    letra_designada = str(input("Qué letra desea ingresar?: "))
    while count < max_valores:
        for _ in range(batch_size):
            datos_completos = []
            batch_data = []
            dato_cortado = []
            dato = ser.readline().decode().strip()
            if dato == "\\n":
                break
            dato_cortado = dato.split(',')
            datos_completos.extend(dato_cortado)
            datos_completos = list(filter(lambda palabra: palabra.strip() != "", datos_completos))
            batch_data.append(persona)
            batch_data.append(letra_designada)
            print(datos_completos)
            for i in datos_completos:
                batch_data.append(i)
            sheet.append(batch_data)
            workbook.save(excel_file_path)
            count += 1
            print(count, " / ", max_valores)

finally:
    # Asegurarse de cerrar el puerto serial al finalizar
    ser.close()